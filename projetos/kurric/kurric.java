package com.kurric.kurric;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.FileOutputStream;
import java.net.MalformedURLException;
import java.net.URL;

public class kurric {
    public static void main(String[] args) {
        System.out.println("Olá!");
        System.out.println("Se está lendo isso você provavelmente é um avançado ser mecatrônico!");
        System.out.println("Este programa irá baixar uma versão atualizada do meu currículo.");

        URL url = null;
        try {
            System.out.println("Baixando agora...");
            url = new URL("https://jlt.codeberg.page/curriculo_atual.pdf");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        try {
            assert url != null;
            try (InputStream inputStream = url.openStream();
                 BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
                 FileOutputStream fileOutputStream = new FileOutputStream("juno_curriculo.pdf")
            ) {
                byte[] bucket = new byte[2048];
                int numBytesRead;

                while ((numBytesRead = bufferedInputStream.read(bucket, 0, bucket.length)) != -1) {
                    fileOutputStream.write(bucket, 0, numBytesRead);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Se tudo correu bem, você deve ter o meu currículo atualizado junto a este arquivo.");
        System.out.println("Caso contrário, o programa pode não ter as permissões necessárias neste local...");
        System.out.println("Se foi um erro no servidor, você também pode achar meu currículo nesse endereço:");
        System.out.println("newen.me/jlt/curriculo.html");
        System.out.println("Gostou do que viu? Entra em contato!");
        System.out.println("juno@newen.me WPP 11 973703486 :)");
    }
}
