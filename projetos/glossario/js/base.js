let glossario = [
    {
        "id": "1",
        "termo": "html",
        "desc": "Linguagem de marcação (?) para estruturar páginas web semanticamente."
    },
    {
        "id": "2",
        "termo": "css",
        "desc": "Linguagem de marcação para web."
    },
    {
        "id": "3",
        "termo": "javascript",
        "desc": "Linguagem de programação orientada a objetos e sem classes."
    },
    {
        "id": "4",
        "termo": "editor",
        "desc": "Programa que permite editar as informações de um arquivo."
    },
    {
        "id": "5",
        "termo": "terminal",
        "desc": "Interface de entrada e saída de dados do tipo texto."
    },
    {
        "id": "6",
        "termo": "linguagem",
        "desc": "Em programação, um conjunto de regras lógicas formais (gramática) que permite a criação de programas."
    },
    {
        "id": "7",
        "termo": "compilar",
        "desc": "Tornar um código executável."
    },
    {
        "id": "8",
        "termo": "renderizar",
        "desc": "A definir"
    },
    {
        "id": "9",
        "termo": "marcação",
        "desc": "A definir"
    },
    {
        "id": "10",
        "termo": "git",
        "desc": "Programa de gerenciamento de versões"
    },
    {
        "id": "11",
        "termo": "ssh",
        "desc": "Protocolo para acesso remoto via terminal."
    },
    {
        "id": "12",
        "termo": "ftp",
        "desc": "Protocolo para envio de arquivos."
    }
]

console.log("Dados carregados. Length: " + glossario.length);

var botao = document.getElementById("botao");
var saida = document.getElementById("saida");
var saida2 = document.getElementById("saida2");

i = 0;

console.log("Variáveis carregadas");

console.log(glossario);

strTeste = glossario[1].termo + " " + glossario[1].desc;
console.log("Teste com index 1: \"" + strTeste + "\"");

function anterior() {
	i--;
	document.getElementById("saida").innerHTML =
	glossario[i].id + '. ' + glossario[i].termo;
	document.getElementById("saida2").innerHTML =
	glossario[i].desc;
}

function proximo() {
	i++;
	document.getElementById("saida").innerHTML =
	glossario[i].id + '. ' + glossario[i].termo;
	document.getElementById("saida2").innerHTML =
	glossario[i].desc;
}

function tudo() {

	for (var i = 0; i < glossario.length; i++) {
					var div = document.createElement("div");
					div.innerHTML = glossario[i].id + ': ' + glossario[i].termo + ' ' + glossario[i].desc;
					saida2.appendChild(div);
				}
				
    return false;
}

document.getElementById('botao-anterior').addEventListener('click', anterior);
document.getElementById('botao-proximo').addEventListener('click', proximo);
document.getElementById('botao-todos').addEventListener('click', tudo);
